# This code will be run for interactive shells.
if status is-interactive

  # Test for .venv/bin/activate.fish in the current active directory.
  set activate_fish_path .venv/bin/activate.fish

  if test -f $activate_fish_path
  # Ask the user for confirmation to source the virtual environment.
  read -P "Activate $activate_fish_path? [y/N] " -n 1 -l activate_confirm
  echo

  # Check the user's response.
  switch $activate_confirm
  case Y y
  # If the user confirms, source the virtual environment.
  source $activate_fish_path
  echo "$activate_fish_path activated."
  end
  else
  # If the file doesn't exist in the active directory, check the home directory.
  set home_activate_fish_path $HOME/.venv/bin/activate.fish

  if test -f $home_activate_fish_path
  # Ask the user for confirmation to source the virtual environment.
  read -P "Activate $home_activate_fish_path? [y/N] " -n 1 -l activate_confirm
  echo

  # Check the user's response.
  switch $activate_confirm
  case Y y
  # If the user confirms, source the virtual environment.
  source $home_activate_fish_path
  echo "$activate_fish_path activated."
  end
  else
  echo "No Virtual Environment found."
  end
  end
  end
